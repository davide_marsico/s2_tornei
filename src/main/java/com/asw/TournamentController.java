package com.asw;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;



/**
* This controller gives information about daily tournaments and their fees
*
* @author  Alessio Buscemi

*/


@Configuration
@PropertySource("application.yml")
@RestController
public class TournamentController {

	
	
	@Value("${tournament.tournamentDay.uri}")
	private String tournamentDayUri;

	
	@Value("${tournament.tournamentFee.uri}")
	private String tournamentFeeUri;
	
	@Value("${tournament.type.uri}")
	private String typeUri;
	
	
	
	
	@RequestMapping("/{giorno}")
	public String getGiornoTorneo(@PathVariable String giorno) {
	
		String grounds = getTournamentDay(giorno);
		
		String res = getTypes(grounds);
		
		return "In questa settimana, " + giorno + ", si svolgono tornei nei seguenti campi: " + res;
		
		
	}

	
	@RequestMapping("/{giorno}/{costo}")
	public String getDisponibilitaGiorno(@PathVariable String giorno,@PathVariable String costo) {

	
		String grounds = getTournamentFee(giorno,costo);
		
		String res = getTypes(grounds);
		
		return "In questa settimana, " + giorno + ", il costo di iscrizione per i tornei, il cui prezzo è inferiore a " + costo + " è: " + res;
		
		
	}

	
	private String getType(String uri,String grounds) {
		return new RestTemplate().getForObject(uri+"/"+grounds,String.class);
	}


	private String getTypes(String grounds) {
		return getType(typeUri,grounds);
	}

	
	
	private String getGroundDay(String uri,String day) {
		return new RestTemplate().getForObject(uri+day,String.class);
	}

	private String getGroundFee(String uri,String day,String fee) {
		return new RestTemplate().getForObject(uri+day+"/"+fee,String.class);
	}
	
	private String getTournamentFee(String day,String fee) {
		return getGroundFee(tournamentFeeUri,day,fee);
	}
	
	

	private String getTournamentDay(String day) {
		return getGroundDay(tournamentDayUri,day);
	}
	
}
